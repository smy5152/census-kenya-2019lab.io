function make_map(cityname, dom_id){
  achart = echarts.init(document.getElementById(dom_id));
  var option =  {
    "title": [
      {
	"textStyle": {
	  "color": "#000",
	  "fontSize": 18
	},
	"subtext": "",
	"text": cityname,
	"top": "auto",
	"subtextStyle": {
	  "color": "#aaa",
	  "fontSize": 12
	},
	"left": "auto"
      }
    ],
    "legend": [
      {
	"selectedMode": "multiple",
	"top": "top",
	"orient": "horizontal",
	"data": [
	  ""
	],
	"left": "center",
	"show": true
      }
    ],
            visualMap: {
            left: 'right',
            min:  0,
            max: 50,
            inRange: {
                color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f46d43', '#d73027', '#a50026']
            },
            text: ['High', 'Low'],          
            calculable: true
        },

    "backgroundColor": "#fff",
    	  tooltip: {
		  trigger: 'item',
		  formatter: function (params) {
			  var value = (params.value + '').split('.');
			  value = value[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,');
			  return params.seriesName + '<br/>' + params.name + ' : ' + value;
		  }
	  },
	  toolbox: {
		  show: true,
		  orient: 'vertical',
		  left: 'right',
		  top: 'center',
        feature: {
            dataView: {readOnly: true},
            restore: {},
            saveAsImage: {}
        }
    },
    "series": [
      {
	"mapType": cityname,
	"data": [
           {name: "Mombasa", value: 28.2},
           {name: "Kwale", value: 9.8},
           {name: "Kilifi", value: 11.6},
           {name: "Tana River", value: 5.5},
           {name: "Lamu", value: 11.8},
           {name: "Taita Taveta", value: 15.9},
           {name: "Garissa", value: 6.3},
           {name: "Wajir", value: 3.5},
           {name: "Mandera", value: 3.5},
           {name: "Marsabit", value: 4.4},
           {name: "Isiolo", value: 8.9},
           {name: "Meru", value: 10.2},
           {name: "Tharaka-Nithi", value: 11.5},
           {name: "Embu", value: 14.7},
           {name: "Kitui", value: 6.8},
           {name: "Machakos", value: 17.6},
           {name: "Makueni", value: 9.4},
           {name: "Nyandarua", value: 12.4},
           {name: "Nyeri", value: 21.2},
           {name: "Kirinyaga", value: 13.6},
           {name: "Murang`a", value: 11.9},
           {name: "Kiambu", value: 39.1},
           {name: "Turkana", value: 4.4},
           {name: "West Pokot", value: 3.5},
           {name: "Samburu", value: 6.8},
           {name: "Trans Nzoia", value: 11.0},
           {name: "Uasin Gishu", value: 20.4},
           {name: "Elegeyo-Marakwet", value: 5.0},
           {name: "Nandi", value: 8.8},
           {name: "Baringo", value: 8.2},
           {name: "Laikipia", value: 17.5},
           {name: "Nakuru", value: 20.9},
           {name: "Narok", value: 6.9},
           {name: "Kajiado", value: 29.1},
           {name: "Kericho", value: 11.1},
           {name: "Bomet", value: 8.0},
           {name: "Kakamega", value: 8.6},
           {name: "Vihiga", value: 8.8},
           {name: "Bungoma", value: 7.2},
           {name: "Busia", value: 7.4},
           {name: "Siaya", value: 8.3},
           {name: "Kisumu", value: 20.0},
           {name: "Homa Bay", value: 8.2},
           {name: "Migori", value: 9.1},
           {name: "Kisii", value: 8.8},
           {name: "Nyamira", value: 7.3},
           {name: "Nairobi", value: 42.1}
    ],
	"name": "Kenya Proportion with Internet (2019)",
	"type": "map",
	"roam": true,
    emphasis: {
				  label: {
					  show:true
				  }
			  }
      },
      
    ]
  };
  achart.setOption(option);
}

